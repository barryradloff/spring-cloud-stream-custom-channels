package demo.stream.multiplechannels;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Payment {

    private String from;
    private double amount;
    private String timestamp = new SimpleDateFormat().format(new Date());

    public Payment(String from, double amount) {
        this.from = from;
        this.amount = amount;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return String.format("[%s] Payment from %s, USD %f", timestamp, from, amount);
    }
}
