package demo.stream.multiplechannels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.SubscribableChannel;

@SpringBootApplication
public class MultiplePaymentConsumerApplication {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "multiplepaymentconsumer");
        SpringApplication.run(MultiplePaymentConsumerApplication.class, args);
    }
}

@EnableBinding(MultiplePaymentConsumer.Channels.class)
class MultiplePaymentConsumer {

    @StreamListener(Channels.LOINPUT)
    public void logLo(Payment message) {
        System.out.println("Lo-value payment: " + message);
    }

    @StreamListener(Channels.HIINPUT)
    public void logHi(Payment message) {
        System.out.println("Hi-value payment: " + message);
    }

    interface Channels {
        String LOINPUT = "loPaymentInput";
        String HIINPUT = "hiPaymentInput";

        @Input(LOINPUT)
        SubscribableChannel loInput();

        @Input(HIINPUT)
        SubscribableChannel hiInput();
    }
}