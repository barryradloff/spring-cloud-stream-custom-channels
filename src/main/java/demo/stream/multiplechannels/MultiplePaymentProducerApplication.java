package demo.stream.multiplechannels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.messaging.MessageChannel;

@SpringBootApplication
public class MultiplePaymentProducerApplication {

    public static void main(String[] args) {
        System.setProperty("spring.config.name", "multiplepaymentproducer");
        SpringApplication.run(MultiplePaymentProducerApplication.class, args);
    }
}

@EnableBinding(MultiplePaymentProducer.Channels.class)
class MultiplePaymentProducer {

    @InboundChannelAdapter(value = Channels.LO_OUTPUT, poller = @Poller(fixedDelay = "5000"))
    public Payment loPaymentMessageSource() {
        System.out.println("Lo Payment running");
        return new Payment("John Doe", Math.random() * 100);
    }

    @InboundChannelAdapter(value = Channels.HI_OUTPUT, poller = @Poller(fixedDelay = "1000"))
    public Payment hiPaymentMessageSource() {
        System.out.println("Hi payment running");
        return new Payment("Mary Doe", 500000 + Math.random() * 100);
    }

    interface Channels {
        String LO_OUTPUT = "loPaymentOutput";
        String HI_OUTPUT = "hiPaymentOutput";

        @Output(LO_OUTPUT)
        MessageChannel loOutput();

        @Output(HI_OUTPUT)
        MessageChannel hiOutput();
    }
}